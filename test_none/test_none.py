from omegaconf import DictConfig, OmegaConf
import hydra

def is_none(cfg: DictConfig, key: str):
  item = OmegaConf.select(cfg, key)
  print(key, ' is ', 'None' if item is None else item.__class__)


@hydra.main(config_path='.', config_name='config')
def main(cfg: DictConfig):
  is_none(cfg, 'none')
  is_none(cfg, 'some_dict.none')
  is_none(cfg, 'some_list.none[0]')


if __name__ == '__main__':
  main()
