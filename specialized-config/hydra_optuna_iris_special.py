#!/usr/bin/python
# -*- encoding: utf-8 -*-

from abc import ABCMeta, abstractmethod, abstractstaticmethod
import os
from typing import Any, Dict, List

from sklearn import datasets, model_selection, metrics
from sklearn.ensemble import RandomForestClassifier

import hydra
from omegaconf import DictConfig, ListConfig

from mlflow.tracking import MlflowClient
import neptune.new as neptune


class Run(metaclass=ABCMeta):
    def __init__(self, name, **kwargs):
        self.experiment_name = name

        class _Proxy:
            def __init__(self, run, type):
                self.run = run
                self.type = type

            def __setitem__(self, key, value):
                if type == 'param':
                    self.run.log_param(key, value)
                elif type == 'metric':
                    self.run.log_metric(key, value)

        self.param = _Proxy(self, 'param')
        self.metric = _Proxy(self, 'metric')

    def __del__(self):
        self.terminate()

    @abstractstaticmethod
    def _init_kwargs() -> List[str]:
        return []

    @staticmethod
    def _filter_init_args(kwargs: Dict[str, Any]) -> Dict[str, Any]:
        return {k: v for k, v in kwargs.items() if k in __class__._init_kwargs()}

    @abstractmethod
    def log_param(self, key, value):
        pass

    @abstractmethod
    def log_metric(self, key, value):
        pass

    def log_params_from_omegaconf_dict(self, conf):
        for k, v in conf.items():
            self._log_params_recursive(k, v)

    def _log_params_recursive(self, parent_name, element):
        if isinstance(element, DictConfig):
            for k, v in element.items():
                key = self._translate_key(k)
                if isinstance(v, DictConfig) or isinstance(v, ListConfig):
                    self._log_params_recursive(f'{parent_name}.{key}', v)
                else:
                    self.log_param(key, v)
        elif isinstance(element, ListConfig):
            for i, v in enumerate(element):
                suffix = self._str_array_suffix(i)
                self.log_param(f'{parent_name}_{suffix}', v)

    @staticmethod
    def _translate_key(key):
        return key

    @staticmethod
    def _str_array_suffix(i):
        return f'_{i}'

    @abstractmethod
    def terminate(self):
        pass


class MlflowRun(Run):
    @staticmethod
    def _init_kwargs():
        return ['tracking_uri', 'registry_uri']

    def __init__(self, name, **kwargs):
        super().__init__(name)

        self.client = MlflowClient(self._filter_init_args(kwargs))
        try:
            self.experiment_id = self.client.create_experiment(name)
        except:
            self.experiment_id = self.client.get_experiment_by_name(
                name).experiment_id

        self.run = self.client.create_run(self.experiment_id)

    def log_param(self, key, value):
        self.client.log_param(self.run.info.run_id, key, value)

    def log_metric(self, key, value):
        self.client.log_metric(self.run.info.run_id, key, value)

    def log_artifact(self, path):
        self.client.log_artifact(self.run.info.run_id, path)

    def terminate(self):
        self.client.set_terminated(self.run.info.run_id)


class NeptuneAiRun(Run):
    def __init__(self, name, project, api_token, tags=[], **kwargs):
        super().__init__(name)
        self.run = neptune.init(project=project, api_token=api_token, name=name, tags=tags, self._filter_init_args(kwargs))

    @staticmethod
    def _init_kwargs():
        return ['run', 'custom_run_id', 'mode', 'description', 'source_files', 'capture_stdout', 'capture_stderr', 'capture_hardware_metrics', 'monitoring_namespace', 'fail_on_exception', 'flush_period', 'proxies']

    def log_param(self, key, value):
        self.run[key] = value
    
    def log_metric(self, key, value):
        self.run[f'metric/{key}'] = value
    
    def terminate(self):
        self.run.stop()


def objective(cfg: DictConfig) -> float:
    # irisデータ読み込み
    iris = datasets.load_iris()
    X_train, X_test, label_train, label_test = model_selection.train_test_split(
        iris.data, iris.target, test_size=0.3, random_state=0)

    # Classifier parameters
    n_estimators = cfg.n_estimators
    criterion = cfg.criterion
    max_depth = None
    min_samples_split = cfg.min_samples_split
    max_features = cfg.max_features
    max_leaf_nodes = None
    random_state = cfg.random_state

    # Train
    clf = RandomForestClassifier(n_estimators=n_estimators,
                                 criterion=criterion,
                                 max_depth=max_depth,
                                 min_samples_split=min_samples_split,
                                 max_features=max_features,
                                 max_leaf_nodes=max_leaf_nodes,
                                 random_state=0)  # 分類器RandomForest
    clf.fit(X_train, label_train)  # 学習

    # Predict
    pre = clf.predict(X_test)

    # Score
    ac_score = metrics.accuracy_score(label_test, pre)
    return float(ac_score)


@hydra.main(config_path='conf', config_name='config')
def main(cfg: DictConfig) -> float:
    # Replace str "None" to None
    for key in cfg:
        if cfg[key] == "None":
            cfg[key] = None

    # Create a run tracker
    run = hydra.utils.instantiate(cfg.tracking)
    run.log_params_from_omegaconf_dict(cfg)
    cwd = os.getcwd()
    if isinstance(run, MlflowRun):
        run.log_artifact(os.path.join(cwd, '.hydra/config.yaml'))
        run.log_artifact(os.path.join(cwd, '.hydra/hydra.yaml'))
        run.log_artifact(os.path.join(cwd, '.hydra/overrides.yaml'))

    # Train
    acc = objective(cfg)
    run.metric['accuracy'] = acc
    run.terminate()

    return acc


if __name__ == '__main__':
    main()
