#!/usr/bin/python
# -*- encoding: utf-8 -*-

from sklearn import datasets, model_selection, metrics
from sklearn.ensemble import RandomForestClassifier

import hydra
from omegaconf import DictConfig, ListConfig


def objective(cfg: DictConfig) -> float:
    # irisデータ読み込み
    iris = datasets.load_iris()
    X_train, X_test, label_train, label_test = model_selection.train_test_split(
        iris.data, iris.target, test_size=0.3, random_state=0)

    # Classifier parameters
    params = {
        'n_estimators': cfg.training.n_estimators,
        'criterion': cfg.training.criterion,
        'max_depth': None,
        'min_samples_split': cfg.training.min_samples_split,
        'max_features': cfg.training.max_features,
        'max_leaf_nodes': None,
        'random_state': cfg.training.random_state
    }

    # Train
    clf = RandomForestClassifier(**params)
    clf.fit(X_train, label_train)  # 学習

    # Predict
    pre = clf.predict(X_test)

    # Score
    ac_score = metrics.accuracy_score(label_test, pre)
    return float(ac_score)


def none_config(cfg: DictConfig):
    for key in cfg:
        _do_none_config(cfg, key)


def _do_none_config(parent, key):
    item = parent[key]
    if isinstance(item, DictConfig) or isinstance(item, ListConfig):
        for key in item:
            _do_none_config(item, key)
    elif item == 'None':
        parent[key] = None


@hydra.main(config_path='.', config_name='config')
def main(cfg: DictConfig) -> float:
    # Replace str "None" to None
    none_config(cfg)
    return objective(cfg)


if __name__ == '__main__':
    main()
