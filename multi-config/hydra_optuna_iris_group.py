#!/usr/bin/python
# -*- encoding: utf-8 -*-

from sklearn import datasets, model_selection, metrics
from sklearn.ensemble import RandomForestClassifier

import hydra
from omegaconf import DictConfig


def objective(cfg: DictConfig) -> float:
    # irisデータ読み込み
    iris = datasets.load_iris()
    X_train, X_test, label_train, label_test = model_selection.train_test_split(
        iris.data, iris.target, test_size=0.3, random_state=0)

    # Classifier parameters
    n_estimators = cfg.n_estimators
    criterion = cfg.criterion
    max_depth = None
    min_samples_split = cfg.min_samples_split
    max_features = cfg.max_features
    max_leaf_nodes = None
    random_state = cfg.random_state

    # Train
    clf = RandomForestClassifier(n_estimators=n_estimators,
                                 criterion=criterion,
                                 max_depth=max_depth,
                                 min_samples_split=min_samples_split,
                                 max_features=max_features,
                                 max_leaf_nodes=max_leaf_nodes,
                                 random_state=0)  # 分類器RandomForest
    clf.fit(X_train, label_train)  # 学習

    # Predict
    pre = clf.predict(X_test)

    # Score
    ac_score = metrics.accuracy_score(label_test, pre)
    return float(ac_score)


@hydra.main(config_path='conf', config_name='config')
def main(cfg: DictConfig) -> float:
    # Replace str "None" to None
    for key in cfg:
        if cfg[key] == "None":
            cfg[key] = None
    return objective(cfg)


if __name__ == '__main__':
    main()
