# README #

[TOC]

## What is this repository for?

* Quick summary
    * HydraとOptunaを使用してMLタスクを実行する際のテンプレート．
    * Templates for running ML tasks using Hydra and Optuna.

* Version
* References
    * [Hydra](https://hydra.cc/)
	* [Optuna](https://optuna.org/)

## How do I get set up?

```
$ pipenv install --dev
```

デモプログラムの動作に必要な以下のパッケージを導入する．

- scikit-learn
- hydra-core
- hydra-optuna-sweeper
- mlflow

## How do I run the script?

### Single config file version

``single-conf``ディレクトリ以下には，単一のpythonスクリプトと設定ファイル``config.yaml``がある．
pythonスクリプトはirisをRandomForestを用いて分類する例題である．

In ``single-conf``, there are a python script and a single config file named ``config.yaml``.

#### HydraとOptunaを組み合わせたパラメータサーチ

以下の参考文献では，HydraとOptunaを組み合わせたハイパーパラメータのサーチについて紹介されているが，情報が古くそのままでは動作しない（2021/7現在．Hydra==1.1.0）．
そのため，これらの記事からの差分を中心に記述する．

##### ``@hydra.main``デコレータの変更

Hydra==1.0から，デコレータ``@hydra/main``のパラメータ[``config_path``の扱いが変更になった](https://hydra.cc/docs/next/upgrades/0.11_to_1.0/config_path_changes/)．``config_path``の指定は必須であり，カレントディレクトリに設定ファイルを置く場合は，``config_path='.'``とする．

##### ``config.yaml``の``hydra/sweeper``の設定

古い情報では，以下のように記述する例がある．

```config.yaml
defaults:
  - hydra/sweeper: optuna
  
hydra:
  sweeper:
    optuna_config:
      study_name: iris
      seed: 123
```

現在のバージョンでは，以下のように``optuna_config``ではなく，``sweeper``以下に設定を記述する．また，``seed``プロパティは，``sampler``の設定に移されている．

```
defaults:
  - override hydra/sweeper: optuna
  
hydra:
  sweeper:
    study_name: iris
    sampler:
      seed: 123
```

##### 探索空間の記述

``config.yaml``の``hydra/sweeper/search_space``に以下のように記述する．

```config.yaml
hydra:
  sweeper:
    ...
    search_space:
      n_estimators:
        type: int
        low: 2
        high: 200
        log: true
      criterion:
        type: categorical
        choices: [gini, entropy]
      min_samples_split:
        type: int
        low: 2
        high: 8
        step: 1
      max_features:
        type: categorical
        choices: [null, auto, sqrt]
```

設定の詳細については，[公式ドキュメント](https://hydra.cc/docs/plugins/optuna_sweeper)参照．ドキュメントに書かれていないことは，直接[ソースファイル](https://github.com/facebookresearch/hydra/blob/master/plugins/hydra_optuna_sweeper/hydra_plugins/hydra_optuna_sweeper/config.py)を読む．

YAMLでの``null``はPythonでは``None``を意味する．Hydraでもそのように扱われるが，``OmegaConf``を介してpythonスクリプトに渡されるのは，``None``ではなく，文字列``'None'``であることに注意する．
single-config/hydra_optuna_iris.py の44行目からの処理で``'None'``を``None``に置換している．

##### 最適化目的関数

single-config/hydra_optuna_iris.py では，``main``関数を``@hydra.main``デコレータで修飾し，学習の処理は``objective``関数に分離している．``objective``関数は，最適化の目的関数（ここではaccuracy）の値を返す．``main``は``objective``の戻り値をそのまま返している．

#### Hydraが設定するパラメータの確認

Hydraがどのようにパラメータを設定したかを確認するには，コマンドラインオプションに``--cfg all``を付ける．
``--cfg all``はスクリプトのタスクを実行することなく，パラメータ設定だけを表示する．

``all``の代わりに``--cfg job``を用いると，ユーザが設定したパラメータだけが表示される．また，``--cfg hydra``では，Hydraに関するパラメータだけが表示される．

特定の[パッケージ](https://hydra.cc/docs/advanced/overriding_packages)のパラメータだけを表示したい場合には，``-p パッケージ名``オプションを用いる．

Add command line option ``--cfg all``; this will show all options set. Instead, ``--cfg job`` presents your own parameters and ``--cfg hydra`` does Hydra's config parameters.

You can get parameters of only a specific package by ``-p <package_name>``.

* References
    * Official document: [Debugging](https://hydra.cc/docs/tutorials/basic/running_your_app/debugging)
    * Official document: [Hydra's command line flags](https://hydra.cc/docs/advanced/hydra-command-line-flags)

例えば，single-config/hydra_optuna_iris.py のユーザパラメータだけを表示したい場合，以下のように行う．

```
$ cd single-config
$ pipenv run python ./hydra_optuna_iris.py --cfg job
```

出力:

```
n_estimators: 2
criterion: gini
max_depth: null
min_samples_split: 2
max_features: null
max_leaf_nodes: 1
random_state: 1
```

##### 探索の実行

``single-config``の例では，以下のようにコマンドラインに``-m``オプションを与えることでパラメータサーチが実行される．

```
$ pipenv run python ./hydra_optuna_iris.py -m
```

出力:

```
[I 2021-07-07 14:42:46,569] A new study created in memory with name: iris
[2021-07-07 14:42:46,570][HYDRA] Study name: iris
[2021-07-07 14:42:46,570][HYDRA] Storage: None
[2021-07-07 14:42:46,570][HYDRA] Sampler: TPESampler
[2021-07-07 14:42:46,570][HYDRA] Directions: ['maximize']
[2021-07-07 14:42:46,591][HYDRA] Launching 2 jobs locally
[2021-07-07 14:42:46,592][HYDRA]        #0 : n_estimators=45 criterion=gini min_samples_split=4 max_features=None random_state=20
[2021-07-07 14:42:47,011][HYDRA]        #1 : n_estimators=51 criterion=gini min_samples_split=5 max_features=auto random_state=91
...
[2021-07-07 14:43:27,978][HYDRA] Launching 2 jobs locally
[2021-07-07 14:43:27,978][HYDRA]        #98 : n_estimators=121 criterion=gini min_samples_split=6 max_features=sqrt random_state=38
[2021-07-07 14:43:28,440][HYDRA]        #99 : n_estimators=127 criterion=gini min_samples_split=6 max_features=sqrt random_state=40
[2021-07-07 14:43:28,900][HYDRA] Best parameters: {'n_estimators': 45, 'criterion': 'gini', 'min_samples_split': 4, 'max_features': None, 'random_state': 20}
[2021-07-07 14:43:28,900][HYDRA] Best value: 0.9777777777777777
```

##### References
- [ハイパラ管理のすすめ -ハイパーパラメータをHydra+MLflowで管理しよう-](https://ymym3412.hatenablog.com/entry/2020/02/09/034644)
- [Hydra, MLflow, Optunaの組み合わせで手軽に始めるハイパーパラメータ管理](https://supikiti22.medium.com/hydra-mlflow-optuna%E3%81%AE%E7%B5%84%E3%81%BF%E5%90%88%E3%82%8F%E3%81%9B%E3%81%A7%E6%89%8B%E8%BB%BD%E3%81%AB%E5%A7%8B%E3%82%81%E3%82%8B%E3%83%8F%E3%82%A4%E3%83%91%E3%83%BC%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF%E7%AE%A1%E7%90%86-6b8e6d41b3da)
- [HydraでOptunaを試す](https://seven-901.hatenablog.com/entry/2021/01/14/160829)

### Multiple config file version

単一の設定ファイルを用いると，ハイパーパラメータサーチが終了した後に，そのパラメータを使って機械学習タスクを行う場合などにプログラムを分ける必要がある．複数の設定ファイルに分けて切り替えて使えるようにする．

#### ディレクトリ構成

[公式ドキュメント](https://hydra.cc/docs/tutorials/basic/your_first_app/config_groups)に従って，``conf``ディレクトリに主設定ファイル``config.yaml``を置く．

その際の``@hydra.main``デコレータのパラメータは以下のようになる．

```python
@hydra.main(config_path='conf', config_name='config')
def main(cfg: DictConfig) -> float:
  pass

if __name__ == '__main__':
  main()
```

#### プラグインの設定

OptunaSweeperプラグインの設定ファイルは，``conf/hydra/sweeper/my_optuna.yaml``に置く．プラグインのパスと同一の階層構造（``hydra/sweeper``）にしなければならいこと，プラグインと同名のファイル名とすることはできないこと（[1.1から不可](https://hydra.cc/docs/next/upgrades/1.0_to_1.1/automatic_schema_matching/)）に留意すること．ここでは，``my_optuna.yaml``とした．

``my_optuna.yaml``には以下を記述する．

```my_optuna.yaml
defaults:
  - optuna

study_name: iris
...
search_space:
  n_estimators:
    ...
```

``defaults``でプラグインの元々の設定ファイルを読み込まなければならない．この際は，``hydra/sweeper=optuna``ではなく，単に``optuna``とする．設定ファイルの階層をプラグインのパスと揃えている（同じパッケージに入れている）ためである．

また，プラグインの設定は，単一ファイルの際は

```yaml
hydra:
  sweeper:
    study_name: iris
    ...
```

のように階層的に記述していたが，本ファイルは元々のプラグインと同じパッケージに属するため，階層は用いずルートに記述する．

* References
    * [公式ドキュメント](https://hydra.cc/docs/patterns/configuring_plugins)

#### ハイパーパラメータサーチ

ハイパーパラメータサーチを行う際には，以下のように実行する．実行結果はsingle-configと同様である．

```
$ cd multi-config
$ pipenv run python hydra_optuna_iris_group.py hydra/sweeper=my_optuna -m
```

#### MLタスクの実行

ハイパーパラメータサーチが完了したら，発見されたパラメータを主設定ファイル``config.yaml``に記述する．

```
n_estimators: 2
criterion: gini
max_depth: null
min_samples_split: 2
max_features: null
max_leaf_nodes: 1
random_state: 1
```

その後，コマンドラインでプラグインを指定せずに実行すれば，このパラメータを用いて本来のMLタスクが実行される．

```
$ pipenv run python hydra_optuna_iris_group.py
```

### さらに実験管理を統合

[ハイパラ管理のすすめ -ハイパーパラメータをHydra+MLflowで管理しよう-](https://ymym3412.hatenablog.com/entry/2020/02/09/034644)のように，Optunaと実験管理ツールの設定をHydraに統合する．

この例では，ハイパーパラメータサーチと本実験で実験管理ツールに異なるタグを記録する．

#### 実験管理ツールの設定

MLFlowと[Neptune.ai](https://neptune.ai/)を切り替える想定で，以下の設定ファイルを作成する．

- conf
    - config.yaml
    - tracking
        - mlflow.yaml
        - neptune.yaml
        - iris_defaults.yaml
    - experiment
        - hyperparam_search.yaml
    - hydra
        - sweeper
            - my_optuna.yaml

主設定ファイル``config.yaml``には以下を追記する．

```config.yaml
defaults:
  - tracking: mlflow
```

MLFlowとNeptune.aiで共用する実験名やデフォルトタグ（本実験を示す``Experiment``）は``tracknig/iris_defaults.yaml``に記載する．

```yaml
name: iris
tags: [Experiment]
```

Neptune.ai用の設定では，``iris_defaults``を読み込んだうえで，プロジェクト名やAPIトークンなどを追加設定する．

```yaml
defaults:
  - iris_defaults

project: "foobar/iris"
api_token: "Token_published_from_Neptune.ai"
```

``experiment``以下には，一つの実験につき一つの設定ファイルを作成する．
ハイパーパラメータサーチ用の設定もここに``hyperparam_search.yaml``として作成する．
ここでは，[公式ドキュメント](https://hydra.cc/docs/patterns/configuring_experiments)に従って，``# @package _global_``を用いた．このようにすると，設定のルートディレクトリ（``conf``）にファイルを置いた場合と同等となり，自パッケージ以外の設定を変更できる．

```yaml
# @package _global_
defaults:
  - override /hydra/sweeper: my_optuna

tracking:
  tags: [Optimize]
```


これらの設定を用いてハイパーパラメータサーチを行うときには，以下のように実行する．

```
$ cd optuna+tracknig
$ pipenv run python hydra_optuna_iris_track.py +experiment=hyperparam_search -m
```

また，例えば``experiment/exp1.yaml``に記述したパラメータで実験を行う場合は，

```
$ pipenv run python hydra_optuna_iris_track.py +experiment=exp1
```

#### 別の方法

[Specializing configuration](https://hydra.cc/docs/patterns/specializing_config)を用いて同じことを実現する例を specialized-config/ に示す．

ここでは，以下のように設定ファイルを作成する．

- conf
    - config.yaml
    - mode
        - normal.yaml
        - hyperparm_search.yaml
    - tracking
        - iris_defaults.yaml
        - mlflow.yaml
        - neptune.yaml
    - tracking_tags
        - hyperparam_search_tags.yaml
    - hydra
        - sweeper
            - my_optuna.yaml

``config.yaml``には以下のように記述し，``mode``によって``tracking/tags``を書き換える．

```yaml
defaults:
  - mode@_here_: normal
  - tracking: mlflow
  - optional tracking_tags@tracking: ${mode}_tags
```

``mode``に指定された値によって，``tracking_tags/${mode}_tags.yaml``がパッケージ``tracking``下に読み込まれる．[公式ドキュメント](https://hydra.cc/docs/advanced/defaults_list)の*Interpolation in the Defaults List*にあるように，この記法は主設定ファイルでしか使えないことに注意．
また，``${}``によって置換が可能なのはグループオプションだけであり，キーは対象にならない．例えば以下の記述はエラーになる．

```yaml
defaults:
  - _self_
  - optional tracking_tags:${mode}_tag
  
mode: normal
```
ここで，``_self_``は自ファイルを``optional``の前に読み込むことを意味する（[公式ドキュメント](https://hydra.cc/docs/advanced/defaults_list)の*Composition order*参照．）

``config.yaml``では，``mode@_here_``とパッケージ指定することによって，``mode``はグローバルパッケージに配置される（[Packages](https://hydra.cc/docs/advanced/overriding_packages)参照）．

ハイパーパラメータサーチを行うときは，以下のように実行する．

```
$ cd specialized-config
$ pipenv run python hydra_optuna_iris_special.py mode=hyperparam_search -m
```

##### 設定によって異なるオブジェクトを生成する．

設定ファイルに``_taget_``キーを与えると，それに設定したクラスのオブジェクトを動的に生成することができる．

specialized-config/hydra_optuna_special.pyの175行目

```
run = hydra.utils.instantiate(cfg.tracking)
```

では，``tracking._target_``に設定されたクラスのコンストラクタに，``tracking``の``_target_``を除いたキーをキーワード引数として渡してオブジェクトを生成する．
例えば，``tracking/mlflow.yaml``では

```ymal
_target_: hydra_optuna_iris_special.MlflowRun
```

としている．

すべてのキーがコンストラクタに渡されるため，コンストラクタが認識できないキーがあるとエラーになる．そのため，この例ではhydra_optuna_iris_special.pyの``Run._filter_init_kwargs()``メソッドで不要なキーをフィルタできるようにしている．

- References
    - https://hydra.cc/docs/advanced/instantiate_objects/overview

## Who do I talk to?

* my+bitbucket@o8r.info