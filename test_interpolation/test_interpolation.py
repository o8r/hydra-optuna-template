import hydra
from omegaconf import DictConfig, ListConfig, OmegaConf


def print_name(path: str, cfg: DictConfig):
  print(f'{path}.name:', cfg.name)


@hydra.main(config_path='.', config_name='config')
def main(cfg: DictConfig):
  print(OmegaConf.to_yaml(cfg))

  print('### Interpolation ###')
  print('dict.name:', cfg.dict.name, OmegaConf.is_interpolation(cfg.dict, 'name'))
  print('list[0]:', cfg.list[0], OmegaConf.is_interpolation(cfg.list, 0))

  print('### Masked copy ###')
  copy = OmegaConf.masked_copy(cfg, cfg.keys())
  print(OmegaConf.to_yaml(copy))

  print('### Indirect interpolation ###')
  print_name('dict', cfg.dict)
  print_name('subconf', cfg.subconf)

  print('### Reassignment ###')
  cfg.dict.name = cfg.dict.name
  print('dict.name:', cfg.dict.name, OmegaConf.is_interpolation(cfg.dict, 'name'))
  cfg.list[0] = cfg.list[0]
  print('list[0]:', cfg.list[0], OmegaConf.is_interpolation(cfg.list, 0))

if __name__ == '__main__':
  main()
